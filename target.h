#ifndef TARGET_H
#define TARGET_H
#include"rect.h"

class Target
{
	public:
		Target(float ix, float iy, int isize);
		void show();	
		void draw();
		void chsize();
		void move();
		void windowcollisions();
		void update();
		void stop();
		rect getRect();
		void drawString(void *font, float x, float y, const char *str);
		void scoreOverlay();
		float getX(){return x;}
		float getY(){return y;}
		float getDX(){return dx;}
		float getDY(){return dy;}
		float getSize(){return size;}
		void rotate();
	private:
		float x, y, size, dx, dy;
		float x2, y2;
		rect r;
		float colT, colS; // T for Target, S for Score
		bool moving;
		int angle;
};
#endif