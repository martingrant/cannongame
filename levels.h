#ifndef LEVELS_H
#define LEVELS_H

#include<iostream>
#include"ball.h"
using namespace std;

class Levels
{
	public:
		Levels(float ix, float iy);
		void show();
		void draw();
	private:
		float x, y;
};
#endif;