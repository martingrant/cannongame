#include"powerbar.h"
#include<glut.h>

Powerbar::Powerbar(float ix, float iy)
{
	x = ix; y = iy;
	bx = x; by = y;
}

void Powerbar::show()
{
	glPushMatrix();
	glTranslatef(x, y, 0);
	glRotatef(0, 0, 0, 1);
	glTranslatef(-x, -y, 0);
	drawbg();
	draw();
	glPopMatrix();
}

void Powerbar::draw()
{
	glColor3f(1, 0, 0);
	glLineWidth(4);
	glBegin(GL_LINES);
	glVertex2f(x, y);
	glVertex2f(x, y+20);
	glEnd();
}

void Powerbar::up()
{
	if(x<140)
		x += 5;
}

void Powerbar::down()
{
	if(x>50)
		x -= 5;
}

void Powerbar::reset()
{
	x = 50;
}

void Powerbar::drawbg()
{
	glColor3f(1, 1, 0);
	glBegin(GL_POLYGON);
	glVertex2f(bx, by);
	glVertex2f(bx, by+20);
	glVertex2f(bx+90, by+20);
	glVertex2f(bx+90, by);
	glEnd();
}

