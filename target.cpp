#include"target.h"
#include<glut.h>
#include"random.h"

Target::Target(float ix, float iy, int isize)
{
	x = ix; y = iy; size = isize;
	x2 = x; y2 = y;
	
	dx=dy = 2;

	colT = 1; colS = 1;

	moving = true;

	angle = 0;
}

void Target::show(){
	glPushMatrix();
	glTranslatef(x, y, 0);
	glRotatef(angle, 0, 0, 1);
	glTranslatef(-x, -y, 0);
	draw();
	glPopMatrix();
}

void Target::draw()
{
	glBegin(GL_POLYGON);
	glColor4f(0, 1, 1, colT);
	glVertex2f(x, y);
	glVertex2f(x, y+size);
	glVertex2f(x+size, y+size);
	glVertex2f(x+size, y);
	glEnd();
}

void Target::update()
{
	show();
	//chsize();
	move();
	windowcollisions();
	r.setRect(x, y, size, size);
	scoreOverlay();
	if(moving==false){colT += -0.02; colS += -0.01; rotate();}
}

void Target::move()
{
	x += dx; x2 += dx;
	y += dy; y2 += dy;
}

void Target::windowcollisions()
{
	if(x > 1000 || x < 0 || x+size > 1000 || x+size < 0)
		dx = -dx;

	if(y > 500 || y < 40 || y+size > 500 || y+size < 40)
		dy = -dy;
}

rect Target::getRect(){ // gets collision box from function and returns it 
	return r;
}

void Target::drawString(void *font, float x, float y, const char *str)	
{																	
	char *ch;														
	glRasterPos3f(x, y, 0.0);										
	for(ch=(char*)str; *ch ; ch++)									
		glutBitmapCharacter(font, (int)*ch);						
}		

void Target::scoreOverlay()
{
	glColor4f(0, 1, 0, colS);
	if(moving==false){drawString(GLUT_BITMAP_HELVETICA_12, x2, y2+10, "+5");}	// draw score text on targets

	y2 += 0.01; // move overlay
	x2 += 0.01; // once target has been hit
}

void Target::stop()
{
	dx=dy=0; 
	moving = false;
}

void Target::chsize()
{
	if(dx == 2)
		//if(size<30){size += 0.2;}
		size += 0.2;
	if(dx == -2)
		size += -0.2;
		//if(size>5){size += -0.2;}
		
}

void Target::rotate()
{
	angle += 3;		
}