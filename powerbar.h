#ifndef POWERBAR_H
#define POWERBAR_H

class Powerbar
{
	public:
		Powerbar(float ix, float iy);
		void show();
		void draw();
		void drawbg();
		void up();
		void down();
		void stop();
		void reset();
	private:
		int x, y, bx, by;
};
#endif;