#include"ball.h"
#include<glut.h>
#include"random.h"
#include<cmath>
#include"cannon.h"

const float PI = 3.14159268;

Ball::Ball(float ix, float iy, int iradius)
{
	x = ix; y = iy; radius = iradius;
	angle = 0;
}

void Ball::show(){
	glPushMatrix();
	glTranslatef(x, y, 0);
	glRotatef(angle, 0, 0, 1);
	glTranslatef(-x, -y, 0);
	draw();
	glPopMatrix();
}

void Ball::draw()
{
	/*glBegin(GL_POLYGON);
	glColor3f(1, 1, 0);
	const int NPOINTS=25;
	const float TWOPI=2*3.1415927;
	const float STEP=TWOPI/NPOINTS;
	for(float angle=0; angle<TWOPI; angle+=STEP)
		glVertex2f(x+radius*cos(angle), y+radius*sin(angle));
	glEnd();*/

	glBegin(GL_POLYGON);
	glColor3f(0, 1.5, 0.5);
	glVertex2f(x-radius, y+radius);
	glVertex2f(x+radius, y+radius);
	glVertex2f(x+radius, y-radius);
	glVertex2f(x-radius, y-radius);
	glEnd();
}


void Ball::gravity()
{
	if(dy != 0)
		dy += GRAVITY;		// to vertical movement
}

void Ball::update()
{
	show();
	move();
	gravity();
	rotate();
	r.setRect(x, y, radius, radius);
}

void Ball::setInit(float idx, float idy)
{
	dx = idx;
	dy = idy;	
}

void Ball::move()
{
	x += dx;	// increment horizontal velocity
	y -= dy;	// inrecrement vertical velocity
}

bool Ball::collidingWith(rect &r){
	if(this->r.intersects(r))
		return true;
	else
		return false;
}

rect Ball::getRect(){ // gets collision box from function and returns it 
	return r;
}

void Ball::rotate(){
	angle += 5;
}
