#ifndef BALL_H
#define BALL_H
#include"rect.h"

const float GRAVITY = 0.003;

class Ball
{
	public:
		Ball(float ix, float iy, int iradius);
		void show();	
		void draw();
		void update();
		void gravity();
		void setInit(float idx, float idy);
		void move();
		float getX(){return x;}
		float getY(){return y;}
		int getRadius(){return radius;}
		rect getRect();
		bool collidingWith(rect &r);
		void rotate();
	private:
		float x, y, radius, dy, dx;
		rect r;
		int angle;
};
#endif