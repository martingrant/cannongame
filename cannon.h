#ifndef CANNON_H
#define CANNON_H

class Cannon
{
	public:
		Cannon(float ix, float iy, float isize);
		void show();
		void draw();
		void rotate(float angle);
		float getRotate(){return nAngle;}
		void powerup();
		void powerdown();
		float getDX(){return dx;}
		float getDY(){return dy;}
		void resetPower(){power = 3;}
	private:
		float x, y, size;
		float dx, dy;
		float nAngle, power;
};
#endif;