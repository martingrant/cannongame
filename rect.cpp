#include "rect.h"

rect::rect()
{
	left=right=top=bottom=0;
}

void rect::setRect(float x, float y, float w, float h)
{
	left = x;
	right = x+w;
	top = y;
	bottom = y+h;
}

bool rect::intersects(rect &r){
	return ! (left > r.right ||
				right < r.left ||
				top > r.bottom ||
				bottom < r.top);
	return true;
}

