#ifndef LOADER_H
#define LOADER_H

class Loader
{
	public:
		Loader();
		void show();
		void draw();
	private:
		float x, y;
};
#endif;