#include"cannon.h"
#include<glut.h>
#include<cmath>

const float PI = 3.14159268;

Cannon::Cannon(float ix, float iy, float isize)
{
	x = ix; y = iy; size = isize;
	nAngle = -30; power = 3;
}

void Cannon::show()
{
	glPushMatrix();
	glTranslatef(x, y, 0);
	glRotatef(nAngle, 0, 0, 1);
	glTranslatef(-x, -y, 0);
	draw();
	glPopMatrix();
}

void Cannon::draw()
{
	glBegin(GL_POLYGON);
		glColor3f(1, 0, 1);
		glVertex2f(x-size, y+size+20);
		glVertex2f(x+size, y+size+20);
		glVertex2f(x+size, y-size);
		glVertex2f(x-size, y-size);
	glEnd();
}

void Cannon::rotate(float angle)
{
	nAngle += angle;
}

void Cannon::powerup()
{
	float angle = nAngle*(PI/180);

	if(power < 11)
	{
		power += (float)0.2;
	}

	angle = -angle + 4.7;

	dx = power*cos(angle);
	dy = power*sin(angle);
}

void Cannon::powerdown()
{
	float angle = nAngle*(PI/180);

	if(power > (float)3)
	{
		power -= 0.2;
	}

	angle = -angle + 5;

	dx = power*cos(angle);
	dy = power*sin(angle);
}