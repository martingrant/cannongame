#include<iostream>
#include <stdlib.h>
#include<glut.h>

#include"random.h"
#include"ball.h"
#include"cannon.h"
#include"powerbar.h"
#include"target.h"
#include<sstream>	
#include<stdexcept>		
#include<string>
#include"rect.h"
#include"levels.h"
#include"loader.h"

using namespace std;

float windowW = 1000, windowH = 500;
const int NTARGETS = 20;

Ball *ball = NULL;
Cannon *cannon = NULL;
Target *target[NTARGETS];
Powerbar *powerbar = NULL;
Levels *currentLevel = NULL;
Loader *loader = NULL;

void drawString(void *font, float x, float y, const char *str)	
{																	
	char *ch;														
	glRasterPos3f(x, y, 0.0);										
	for(ch=(char*)str; *ch ; ch++)									
		glutBitmapCharacter(font, (int)*ch);						
}	

std::string IntToStr(int n)		
{									
  std::ostringstream result;		 
  result << n;						
  return result.str();			
}									
	
void ground()
{
	glBegin(GL_LINE_LOOP);		// top line
	glLineWidth(4);
	glColor3f(1, 0, 0);
	glVertex2f(0, 40);
	glVertex2f(1000, 40);
	glEnd();

	glBegin(GL_POLYGON);
	glColor3f(0.2, 0.5, 0.9);	// main body
	glVertex2f(0, 0);
	glVertex2f(0, 40);
	glVertex2f(1010, 40);
	glVertex2f(1010, 0);
	glEnd();
}

void init()
{
	cannon = new Cannon(20, 68, 8);
	//target = new Target(rnd(100, 900), rnd(80, 450), 20);
	powerbar = new Powerbar(50, 10);

	for(int i=0; i<NTARGETS; i++){
		int isize = 20;//rnd(5, 30);
		int ix = rnd(100, 900);
		int iy = rnd(80, 450);

		target[i] = new Target(ix, iy, isize);
	}
}

int remainingTargets()
{
	int number = NTARGETS;

	for(int i=0; i<NTARGETS; i++){
		if(target[i]->getDY()==0)
			number = number - 1;
	}

	return number;
}

int score()
{
	int score = 0;

	for(int i=0; i<NTARGETS; i++){
		if(target[i]->getDY()==0)
			score = score + 1;
	}

	return score;
}

void messages()
{
	glColor3f(1, 0, 0);
	drawString(GLUT_BITMAP_HELVETICA_12, 8, 15, "Power: ");
	drawString(GLUT_BITMAP_HELVETICA_12, 200, 15, IntToStr(cannon->getRotate()+90).c_str());
	drawString(GLUT_BITMAP_HELVETICA_12, 160, 15, "Angle: ");	
	drawString(GLUT_BITMAP_HELVETICA_12, 260, 15, "Score: ");
	drawString(GLUT_BITMAP_HELVETICA_12, 300, 15, IntToStr(score()).c_str());
	drawString(GLUT_BITMAP_HELVETICA_12, 350, 15, "Remaining Targets: ");
	drawString(GLUT_BITMAP_HELVETICA_12, 460, 15, IntToStr(remainingTargets()).c_str());
}

void detectCollisions(Ball *ball, Target *target[NTARGETS])
{
	for(int i=0; i<NTARGETS; i++)
		if(ball->getRect().intersects(target[i]->getRect()))
			target[i]->stop();
			cout << "collision";
}

void display(void)
{
	glEnable(GL_BLEND);									// enable
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);	// transparency
	glClear(GL_COLOR_BUFFER_BIT);

	ground();
	messages();

	cannon->show();
	if(powerbar)powerbar->show();
	if(ball){ball->show();}
	

	for(int i=0; i<NTARGETS; i++){
		target[i]->show();
		if(target[i]->getDY()==0)
			target[i]->scoreOverlay();
	}
	
	glutSwapBuffers();
}

void update()
{
	for(int i=0; i<NTARGETS; i++)	// update targets
		target[i]->update();

	if(ball){						// update ball, detect collisions
		ball->update();
		detectCollisions(ball, target);
		if(ball->getX() > 1000 || ball->getX() < 0 || ball->getY() > 500 || ball->getY()-ball->getRadius() <= 40 ){delete ball; ball = NULL;}
	}
}

void keysdown(unsigned char key, int x, int y)		// NORMAL KEYS
{
	switch(key)
	{
		case 'a':				// LEFT
			if(cannon->getRotate() > -10)
				cannon->rotate(0);
			else
				cannon->rotate(5);
			break;
		case 'd':				// RIGHT
			if(cannon->getRotate() < -85)
				cannon->rotate(0);
			else
				cannon->rotate(-5);
			break;
		case 'f':				// FIRE BALL
				if(!ball){
					ball = new Ball(20, 68, 5);
					ball->setInit(cannon->getDX(), cannon->getDY());
					powerbar->reset();
					cannon->resetPower();
				}
			break;
		/*case 'c':
			if(ball)delete ball; ball = NULL;
			if(powerbar)delete powerbar; powerbar = NULL;
			if(cannon)delete cannon; cannon = NULL;
			for(int i=0; i<NTARGETS; i++){
				delete target[i];}
			break;*/
		case 27:				// ESCAPE KEY EXIT
			exit(0);
			break;
	}
}

void special_keys(int key, int x, int y)	// ARROW KEYS
{
	switch(key) 
	{
		case GLUT_KEY_RIGHT:				// ARROW RIGHT
			cannon->powerup();
			if(powerbar)powerbar->up();
			break;
		case GLUT_KEY_LEFT:					// ARROW LEFT
			cannon->powerdown();
			if(powerbar)powerbar->down();
			break;
	}
}

void timer(int value)
{
	display();
	update();

	glutTimerFunc(20, timer, 0);
}

void main(int argc, char **argv)
{
	glutInit(&argc, argv);
	init();
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize((int)windowW,(int)windowH);
	glutCreateWindow("Lunar Ball");
	gluOrtho2D(0, windowW, 0, windowH);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glutDisplayFunc(display);
	glutTimerFunc(20, timer, 0);
	glutIdleFunc(display);
	glutKeyboardFunc(keysdown);
	glutSpecialFunc(special_keys);
	glutMainLoop(); 
}